package ru.lab1.Entity;

import lombok.Data;

import java.util.List;

@Data
public class Question {
    private String Name;
    private List<Answer> ListAnswer;
    private String CorrectAnswer;
}
