package ru.lab1;

import org.springframework.context.ApplicationContext;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.lab1.Service.MainServiceImpl;

import java.io.IOException;

public class main {

    public static void main(String [] args) throws IOException {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        MainServiceImpl service = context.getBean(MainServiceImpl.class);
        service.run();
    }
}
