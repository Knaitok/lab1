package ru.lab1.DAO;

import ru.lab1.Entity.Question;

import java.io.IOException;
import java.util.List;

public interface IQuestionDAO {
    Boolean Connection();
    List<Question> ListQuestion() throws IOException;
}
