package ru.lab1.DAO;

import au.com.bytecode.opencsv.CSVReader;
import ru.lab1.Entity.Answer;
import ru.lab1.Entity.Question;
import ru.lab1.Service.MainServiceImpl;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class QuestionDAOImpl implements IQuestionDAO {

    private final String FILENAME = "Question.csv";

    @Override
    public Boolean Connection() {
        try {
            getClass().getClassLoader().getResource(FILENAME).getFile();
        } catch (NullPointerException e) {
            System.out.println("Ошибка подключения");
            return false;
        }
        return true;
    }

    @Override
    public List<Question> ListQuestion() throws IOException {
        List<Question> List = new ArrayList<Question>();
        if(Connection()){
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource(FILENAME).getFile());
            CSVReader reader = new CSVReader(new FileReader(file));
            String[] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                if (nextLine != null) {
                    Question question = new Question();
                    question.setName(nextLine[0]);
                    question.setCorrectAnswer(nextLine[1]);
                    List<Answer> answers = new ArrayList<Answer>();
                    for (int i =2; i<nextLine.length; i++){
                        Answer answer = new Answer();
                        answer.setName(nextLine[i]);
                        answers.add(answer);
                    }
                    question.setListAnswer(answers);
                    List.add(question);
                }
            }
            reader.close();
        }else{
            return null;
        }
        return List;
    }
}
