package ru.lab1.Service;

import ru.lab1.DAO.QuestionDAOImpl;
import ru.lab1.Entity.Answer;
import ru.lab1.Entity.Question;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import static java.lang.System.in;


public class MainServiceImpl implements IMainService {

    private final QuestionDAOImpl questionsDAO;

    public MainServiceImpl(QuestionDAOImpl questionDAO) {
        this.questionsDAO = questionDAO;
    }

    public void run() throws IOException {
        int AnswerTrue =0;
        List<Question> list = questionsDAO.ListQuestion();
        if(list != null){
            for(Question question:list){
                System.out.println(question.getName());
                int countAnswer=1;
                for(Answer answer:question.getListAnswer()){
                    System.out.println(countAnswer+". "+answer.getName());
                    countAnswer++;
                }
                boolean answer = AnswerUser(question);
                if(answer) AnswerTrue++;
            }
            System.out.println("Правильных ответов "+AnswerTrue+" из "+list.size());
        }
    }
    public boolean AnswerUser(Question question){
        System.out.println("Выберите вариант ответа");
        try{
            Scanner scanner = new Scanner(in);
            int numberAnswer = scanner.nextInt();
            if(numberAnswer <= question.getListAnswer().size()){
                if(question.getListAnswer().get(numberAnswer-1).getName().equals(question.getCorrectAnswer())){
                    System.out.println("Правильный ответ");
                    return true;
                }else{
                    System.out.println("Неправильный ответ");
                    return false;
                }
            }else{
                throw new Exception();
            }
        }catch (Exception e){
            System.out.println("Такого ответа не существует");
            boolean answer = AnswerUser(question);
            if(answer){
                return true;
            }else{
                return false;
            }
        }
    }
}
