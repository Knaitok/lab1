package ru.lab1.Service;

import ru.lab1.Entity.Question;

import java.io.IOException;

public interface IMainService {
    void run() throws IOException;
    boolean AnswerUser(Question question);
}
